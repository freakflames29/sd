import React from 'react'
const Ava=(props)=>
{
  return (

      <div className='sty'>
      <img src={`https://joeschmoe.io/api/v1/${props.id}`} alt='avatar'/>
      <h2>{props.name}</h2>
      <p>{props.job}</p>
      </div>

  );
}
export default Ava;
