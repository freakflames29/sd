import React, { Component } from 'react'
import './Avatar.css';
import Ava from './AvatarList';

class A extends Component
{
  constructor()
  {
    super();
    this.state={
      ti:"i am a coder",
      buttn:"Are you clicking me?"
    }

  }
  changeTitle=()=>
  {
    this.setState(
      {
        ti:"i am a coder and a web guy",
        buttn:"You clicked me !"

      })
  }

  render()
  {
    const arr=[

            {
              id:1,
              name:"Sourav",
              job:"Web coder"
            },
            {
              id:2,
              name:"Toton",
              job:"Programmer"
            },
            {
              id:3,
              name:"Nobita",
              job:"writer"
            },
            {
              id:4,
              name:"Mark zuck",
              job:"CEO"
            },
            {
              id:5,
              name:"Jeff bezos",
              job:"amazon"
            }



    ]
    const mapped = arr.map((card,i)=>
  {
    return <Ava id = {arr[i].id} name={arr[i].name} job={arr[i].job}/>;


  })

    return(
              <div className='don'>
                  <h1>{this.state.ti}</h1>
                  {mapped}
                  <button onClick={this.changeTitle}>{this.state.buttn}</button>
              </div>
    );
  }
}


export default A;
