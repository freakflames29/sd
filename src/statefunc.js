import React,{useState} from 'react'
import Dod from './Some';

const Disco=()=>
{
  const [personstate,setState]=useState({person:[
    {name:"Sourav Das",age:20},
    {name:"Jesse Eisenberg",age:36}
  ]
});

const [count,setCount]=useState(1);
const increase=()=>
{
  setCount(count+1);

}
const buttonHandler=()=>
{
  setState({
    person:[
      {name:"Toton Das",age:20},
      {name:"Emma Eisenberg",age:36}
    ]
  })

}

return(
  <div>
      <h1>This is a heading</h1>
      <button onClick={buttonHandler}>Switch Name</button>
      <button onClick={increase}>click to change</button>
      <h1>{count}</h1>
      <Dod name={personstate.person[0].name} age={personstate.person[0].age}/>
      <Dod name={personstate.person[1].name} age={personstate.person[1].age}/>

  </div>

);
}
export default Disco;
